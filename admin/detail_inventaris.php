<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "ujikom";
 
    // membuat koneksi
    $koneksi = new mysqli($servername, $username, $password, $dbname);
 
    // melakukan pengecekan koneksi
    if ($koneksi->connect_error) {
        die("Connection failed: " . $koneksi->connect_error);
    } 
 
    if($_POST['rowid']) {
        $id_inventaris = $_POST['rowid'];
        // mengambil data berdasarkan id
    $sql= "select * from inventaris";
	$result=$koneksi->query($sql);
	foreach ($result as $baris){
		?>
            <table class="table">
				 <tr>
                    <td>ID Inventaris</td>
                    <td>:</td>
                    <td><?php echo $baris['id_inventaris']; ?></td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td><?php echo $baris['nama']; ?></td>
                </tr>
                <tr>
                    <td>Kondisi</td>
                    <td>:</td>
                    <td><?php echo $baris['kondisi']; ?></td>
                </tr>
				 <tr>
                    <td>Keterangan</td>
                    <td>:</td>
                    <td><?php echo $baris['keterangan_inventaris']; ?></td>
                </tr>
				<tr>
                    <td>Jumlah</td>
                    <td>:</td>
                    <td><?php echo $baris['jumlah']; ?></td>
                </tr>
				<tr>
                    <td>ID Jenis</td>
                    <td>:</td>
                    <td><?php echo $baris['id_jenis']; ?></td>
                </tr>
				<tr>
                    <td>Tanggal Register</td>
                    <td>:</td>
                    <td><?php echo $baris['tanggal_register']; ?></td>
                </tr>
				<tr>
                    <td>ID Ruang</td>
                    <td>:</td>
                    <td><?php echo $baris['id_ruang']; ?></td>
                </tr>
				<tr>
                    <td>Kode Inventaris</td>
                    <td>:</td>
                    <td><?php echo $baris['kode_inventaris']; ?></td>
                </tr>
				<tr>
                    <td>ID Petugas</td>
                    <td>:</td>
                    <td><?php echo $baris['id_petugas']; ?></td>
                </tr>
            </table>
        <?php 
 
        }
    }
    $koneksi->close();
?>