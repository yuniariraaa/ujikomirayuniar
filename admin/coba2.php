<html>
 <head>
  <title>Lupa password</title>
  <link rel="stylesheet" href="login.css">
 </head>
 <body>
  <div class="box-login">
   <h2>Reset Password</h2>
   <form action="proses-reset.php" method="POST">
    <div class="inputan">
     <input type="text" name="username" required="">
     <label>Username</label>
    </div>
    <div class="inputan">
     <input type="password" name="password" required="">
     <label>Password Baru</label>
    </div>
    <div class="inputan">
     <input type="password" name="repassword" required="">
     <label>Konfirmasi Password Baru</label>
    </div>
    <input type="submit" class="btnLogin" name="btnReset" value="Reset">
   </form>
  </div>
 </body>
</html>
