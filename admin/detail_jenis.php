<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "ujikom";
 
    // membuat koneksi
    $koneksi = new mysqli($servername, $username, $password, $dbname);
 
    // melakukan pengecekan koneksi
    if ($koneksi->connect_error) {
        die("Connection failed: " . $koneksi->connect_error);
    } 
 
    if($_POST['rowid']) {
        $id_jenis = $_POST['rowid'];
        // mengambil data berdasarkan id
    $sql= "select * from jenis";
	$result=$koneksi->query($sql);
	foreach ($result as $baris){
		?>
            <table class="table">
				<tr>
                    <td>ID Jenis</td>
                    <td>:</td>
                    <td><?php echo $baris['id_jenis']; ?></td>
                </tr>
                <tr>
                    <td>NamaJenis</td>
                    <td>:</td>
                    <td><?php echo $baris['nama_jenis']; ?></td>
                </tr>
                <tr>
                    <td>Kode Jenis</td>
                    <td>:</td>
                    <td><?php echo $baris['kode_jenis']; ?></td>
                </tr>
				<tr>
                    <td>Keterangan</td>
                    <td>:</td>
                    <td><?php echo $baris['keterangan']; ?></td>
                </tr>
            </table>
        <?php 
 
        }
    }
    $koneksi->close();
?>