<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "ujikom";

    // membuat koneksi
    $koneksi = new mysqli($servername, $username, $password, $dbname);

    // melakukan pengecekan koneksi
    if ($koneksi->connect_error) {
        die("Connection failed: " . $koneksi->connect_error);
    } 

    if($_POST['rowid']) {
        $id = $_POST['rowid'];
        // mengambil data berdasarkan id
        $sql = "SELECT * FROM peminjaman";
        $result = $koneksi->query($sql);
        foreach ($result as $baris) { ?>
            <table class="table">
                <tr>
                    <td>ID Peminjaman</td>
                    <td>:</td>
                    <td><?php echo $baris['id_peminjaman']; ?></td>
                </tr>
                <tr>
                    <td>Tanggal Pinjam</td>
                    <td>:</td>
                    <td><?php echo $baris['tanggal_pinjam']; ?></td>
                </tr>
                <tr>
                    <td>Tanggal Kembali</td>
                    <td>:</td>
                    <td><?php echo $baris['tanggal_kembali']; ?></td>
                </tr>
				<tr>
                    <td>Status Peminjaman</td>
                    <td>:</td>
                    <td><?php echo $baris['status_peminjaman']; ?></td>
                </tr>
				<tr>
                    <td>Tanggal Kembali</td>
                    <td>:</td>
                    <td><?php echo $baris['tanggal_kembali']; ?></td>
                </tr>
            </table>
        <?php 

        }
    }
    $koneksi->close();
?>